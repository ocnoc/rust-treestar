#[derive(Eq, PartialEq, Hash)]
pub struct GenerationalIndex {
    index: usize,
    generation: u64
}

